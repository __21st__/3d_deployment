# Demo 3D Model

Steps to setup:

1. Install dependencies "`npm i`"
2. Replace the "`./public/truck.gltf`"
   1. If the filename changes, modify "`MODEL_PATH`" variable at the "`./main.js`" row number 5.
3. Build static files "`npm run build`"
4. Copy "`dist`" directory content to the webserver path e.g. "`/var/www/html/demo_3d`". Note! This path should be publicly available at "`http://your-fqdn.example.com/demo_3d`" to work.
